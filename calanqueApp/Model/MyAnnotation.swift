//
//  MyAnnotation.swift
//  calanqueApp
//
//  Created by pppcFO1 on 22/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit
import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var calanque: Calanque
    var title: String?
    
    init(_ calanque: Calanque) {
        self.calanque = calanque
        self.coordinate = self.calanque.coordinates
        self.title = self.calanque.name
    }
    
}
