//
//  CalanqueTableCell.swift
//  calanqueApp
//
//  Created by pppcFO1 on 18/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class CalanqueTableCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var calanqueIV: RoundImage!
    @IBOutlet weak var nameLabel: UILabel!
    
    var calanque: Calanque!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(_ calanque: Calanque){
        self.calanque = calanque
        containerView.layer.cornerRadius = 20
        containerView.backgroundColor = UIColor.lightGray
        nameLabel.text = self.calanque.name
        calanqueIV.image = self.calanque.image
    }
}
