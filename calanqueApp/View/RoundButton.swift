//
//  RoundButton.swift
//  calanqueApp
//
//  Created by pppcFO1 on 21/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class RoundButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        layer.cornerRadius = 10
    }

}
