//
//  CalanqueCustomTableCell.swift
//  calanqueApp
//
//  Created by pppcFO1 on 18/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class CalanqueCustomTableCell: UITableViewCell {

    @IBOutlet weak var calanqueIV: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    var calanque: Calanque? {
        didSet {
            if calanque != nil {
                calanqueIV.image = calanque!.image
                nameLabel.text = calanque!.name
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
