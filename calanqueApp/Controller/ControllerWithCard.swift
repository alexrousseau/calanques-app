//
//  ControllerWithCard.swift
//  calanqueApp
//
//  Created by Alex Rousseau on 17/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

/**
 * For real it is Controller W/ MAP and not Card, bad translation from me
 */

import UIKit
import MapKit

class ControllerWithCard: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    var userLocation: CLLocation?
    var calanques: [Calanque] = CalanqueCollection().all()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        addAnnotation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifDetail), name: Notification.Name("detail"), object: nil)
        
        if calanques.count > 0 {
            let firstOneCoordinates = calanques[0].coordinates
            
            setupMap(coordinates: firstOneCoordinates)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            if let myLocation = locations.last {
                userLocation = myLocation
            }
        }
    }
    
    func setupMap(coordinates: CLLocationCoordinate2D) {
        // + c'est grand + nous sommes éloignés de la map
        // 0.01 risque de zoomé jusqu'à ce qu'on voit une rue
        let span = MKCoordinateSpan(latitudeDelta: 0.35, longitudeDelta: 0.35)
        
        let region = MKCoordinateRegion(center: coordinates, span: span)
        
        mapView.setRegion(region, animated: true)
    }
    
    @objc func notifDetail(notification: Notification) {
        if let calanque = notification.object as? Calanque {
            toDetail(calanque: calanque)
        }
    }
    
    func toDetail(calanque: Calanque) {
        performSegue(withIdentifier: "Detail", sender: calanque)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail" {
            if let controller = segue.destination as? DetailController {
                controller.receivedCalanque = sender as? Calanque
            }
        }
    }
    
    func addAnnotation() {
        for calanque in calanques {
            
            // Basic Annotation
//            let annotation = MKPointAnnotation()
//            annotation.coordinate = calanque.coordinates
//            annotation.title = calanque.name
//            mapView.addAnnotation(annotation)
            
            // Custom Annotation
            let annotation = MyAnnotation(calanque)
            mapView.addAnnotation(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "reuseId"
        
        // we don't touch to the user annotation
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        if let anno = annotation as? MyAnnotation {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
            
            if annotationView == nil {
                
                // Override
                // annotationView = MyAnnotationView(annotation: anno, reuseIdentifier: reuseIdentifier)
                
                annotationView = MyAnnotationView(controller: self, annotation: anno, reuseIdentifier: reuseIdentifier)
                
//                annotationView = MKAnnotationView(annotation: anno, reuseIdentifier: reuseIdentifier)
//                annotationView?.image = UIImage(named: "placeholder")
//                annotationView?.canShowCallout = true
                return annotationView
            } else {
                return annotationView
            }
            
            
        }
        
        return nil
    }
    
    @IBAction func getPosition(_ sender: Any) {
        if userLocation != nil {
            setupMap(coordinates: userLocation!.coordinate)
        }
    }
    
    @IBAction func segmentedChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            mapView.mapType = MKMapType.standard
        case 1:
            mapView.mapType = .satellite
        case 2:
            mapView.mapType = .hybrid
        default: break
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
