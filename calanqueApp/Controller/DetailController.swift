//
//  DetailController.swift
//  calanqueApp
//
//  Created by pppcFO1 on 18/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class DetailController: UIViewController {


    @IBOutlet weak var myCalanqueIV: RoundImage!
    @IBOutlet weak var nameAndDesc: UITextView!
    
    var receivedCalanque: Calanque?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let calanque = receivedCalanque else { return }
        
        myCalanqueIV.image = calanque.image
        
        let mutable = NSMutableAttributedString(string: calanque.name + "\n\n",
                                                attributes: [
                                                    .foregroundColor: UIColor.red,
                                                    .font: UIFont.boldSystemFont(ofSize: 30)
            ])
        
        mutable.append(NSAttributedString(string: calanque.desc,
                                          attributes: [
                                            .font: UIFont.systemFont(ofSize: 17),
                                            .foregroundColor: UIColor.darkGray
                                            
            ]))
        
        nameAndDesc.attributedText = mutable
        nameAndDesc.textAlignment = .center
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
